package com.noon.breakoutScoreSinkConnector;

public final class Constants {
    public static String insertedFlagValue = "insert";
    public static String deletedFlagValue = "delete";
    public static String dataId = "id";
}
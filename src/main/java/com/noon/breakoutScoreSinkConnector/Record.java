package com.noon.breakoutScoreSinkConnector;

import com.google.gson.JsonArray;

public class Record {
    private final JsonArray dataArray;

    public Record(JsonArray dataList) {
        this.dataArray = dataList;
    }
    public JsonArray getDataList() {
        return dataArray;
    }
}

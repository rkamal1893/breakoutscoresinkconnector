package com.noon.breakoutScoreSinkConnector.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserChoiceDetails {

    @JsonProperty("user_id")
    Long userId;

    @JsonProperty("time_to_answer")
    Long timeToAnswer;
}

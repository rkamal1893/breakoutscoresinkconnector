package com.noon.breakoutScoreSinkConnector.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BreakoutTeamResultResponseDto {

    private Long teamId;

    private Long correctChoice;

    Map<Long, List<UserChoiceDetails>> choicesOfUsers;

    private Long teamScore;

}

package com.noon.breakoutScoreSinkConnector.service;

import com.google.common.collect.Lists;
import com.noon.breakoutScoreSinkConnector.SinkConnectorConfig;
import com.google.gson.*;
import com.noon.breakoutScoreSinkConnector.Record;
import com.noon.breakoutScoreSinkConnector.model.BreakoutTeamResultResponseDto;
import com.noon.breakoutScoreSinkConnector.model.UserChoiceDetails;
import com.noon.commons.config.RedisSingleServerConfig;
import org.apache.kafka.connect.json.JsonConverter;
import com.noon.commons.redis.RedisQueryHelper;
import org.redisson.client.codec.LongCodec;
import org.redisson.client.codec.StringCodec;
import org.redisson.codec.JsonJacksonCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class ServiceImpl implements Service {
    private Gson gson;
    private RedisQueryHelper redisQueryHelper;
    private RedisSingleServerConfig redisSingleServerConfig;

    private static Logger log = LoggerFactory.getLogger(ServiceImpl.class);

    public ServiceImpl(SinkConnectorConfig config) {
        PrepareJsonConverters();

        try {
                redisSingleServerConfig= new RedisSingleServerConfig();
                redisSingleServerConfig.setAddress(config.getRedisAddress());
                redisSingleServerConfig.setConnectionMinimumIdleSize(config.getRedisConnectionMinimunIdleSize());
                redisSingleServerConfig.setConnectionPoolSize(config.getRedisConnectionpoolsize());
                redisSingleServerConfig.setConnectTimeout(config.getRedisConnectTimeout());
                redisSingleServerConfig.setDatabase(config.getRedisDatabase());
                redisSingleServerConfig.setIdleConnectionTimeout(config.getRedisIdleconnectionTimeout());
                redisSingleServerConfig.setPassword(config.getRedisPassword());
                redisSingleServerConfig.setRetryAttempts(config.getRedisRetryAttempts());
                redisSingleServerConfig.setSubscriptionsPerConnection(config.getRedisSubscriptionPerConnection());
                redisSingleServerConfig.setThreads(config.getRedisThreads());
                redisSingleServerConfig.setTimeout(config.getRedisTimeout());
                log.info("Redis config {}",redisSingleServerConfig);
                redisQueryHelper = RedisQueryHelper.getInstance(redisSingleServerConfig);
            } catch (Exception e) {
                log.error("The host is unknown, exception stacktrace: {}" , e);
        }
    }

    @Override
    public void process(Collection<String> recordsAsString){
        List<Record> recordList = new ArrayList<>();

        recordsAsString.forEach(recordStr -> {
            try {
                JsonObject recordAsJson = gson.fromJson(recordStr, JsonObject.class);
                log.info("{}",recordAsJson);
                Long correctChoiceId= recordAsJson.get("correct_choice_id").getAsLong();
                String teamId = recordAsJson.get("team_id").getAsString();
                Long breakoutNo = recordAsJson.get("breakout_no").getAsLong();
                Long sessionId = recordAsJson.get("session_id").getAsLong();

                Set<Object> activeTeamMember= redisQueryHelper.getRedissonClient().getSet(teamId + ":breakoutUserList", LongCodec.INSTANCE).readAll();
                BreakoutTeamResultResponseDto breakoutTeamResultResponseDto = (BreakoutTeamResultResponseDto) redisQueryHelper.getRedissonClient().getBucket(sessionId+":"+teamId +":"+breakoutNo+ ":breakoutResultDetails", JsonJacksonCodec.INSTANCE).get();;

                if(activeTeamMember.size()==0 || breakoutTeamResultResponseDto == null){
                    redisQueryHelper.getRedissonClient().getScoredSortedSet(sessionId+":breakoutLeaderBoard", StringCodec.INSTANCE).addScore(teamId,0L);
                }else{
                    List<String> userIdsSelectedSomething = populateAllUserIds(breakoutTeamResultResponseDto.getChoicesOfUsers());
                    List<UserChoiceDetails> correctUserIds  = breakoutTeamResultResponseDto.getChoicesOfUsers().get(correctChoiceId) == null ? Lists.newArrayList() : breakoutTeamResultResponseDto.getChoicesOfUsers().get(correctChoiceId);
                    Long totalCorrectUser = correctUserIds.stream()
                            .filter(userChoiceDetails -> activeTeamMember.contains(userChoiceDetails.getUserId()))
                            .count();
                    Long updatedTeamSize= activeTeamMember.stream()
                            .filter(activeuserId -> userIdsSelectedSomething.contains(activeuserId.toString()))
                            .count();
                    Long team_score = updatedTeamSize.equals(0L)?0L: (300L / updatedTeamSize) * totalCorrectUser;
                    redisQueryHelper.getRedissonClient().getScoredSortedSet(sessionId+":breakoutLeaderBoard", StringCodec.INSTANCE).addScore(teamId,team_score);
                }
            }
            catch (JsonSyntaxException e) {
                log.error("Cannot deserialize json string, which is : " + recordStr);
            }
            catch (Exception e) {
                log.error("Cannot process data, which is : " + recordStr);
            }
        });

        try {
            //redis operation
        }
        catch (Exception e) {
            log.error("Something failed, here is the error: {}",e);
        }
    }

    @Override
    public void closeClient() throws IOException {

    }

    private void PrepareJsonConverters() {
        JsonConverter converter = new JsonConverter();
       // converter.configure(Collections.singletonMap("schemas.enable", "false"), false);

        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
                .create();
    }

    private List<String> populateAllUserIds(Map<Long, List<UserChoiceDetails>> choicesOfUsers) {
        List<String> userIds= new ArrayList<>();
        for (Map.Entry<Long, List<UserChoiceDetails>> entry :choicesOfUsers.entrySet()) {
            for(UserChoiceDetails userChoiceDetails: entry.getValue()) {
                userIds.add(userChoiceDetails.getUserId().toString());
            }
        }
        return userIds;
    }
}

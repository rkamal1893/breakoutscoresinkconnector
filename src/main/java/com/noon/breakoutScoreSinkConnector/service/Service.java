package com.noon.breakoutScoreSinkConnector.service;

import java.io.IOException;
import java.util.Collection;

public interface Service {
    void process(Collection<String> recordsAsString);
    void closeClient() throws IOException;
}

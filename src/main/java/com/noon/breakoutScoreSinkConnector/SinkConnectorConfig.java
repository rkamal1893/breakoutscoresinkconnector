package com.noon.breakoutScoreSinkConnector;

import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.ConfigDef.Importance;

import java.util.Map;


public class SinkConnectorConfig extends AbstractConfig {

  public static final String REDIS_CONNECT_TIMEOUT = "redis.connectTimeout";
  private static final String REDIS_CONNECT_TIMEOUT_DOC = "REDIS connect timeout.";

  public static final String REDIS_TIMEOUT = "redis.timeout";
  private static final String REDIS_TIMEOUT_DOC = "REDIS connect timeout.";

  public static final String REDIS_IDLECONNECTION_TIMEOUT = "redis.idleConnectionTimeout";
  private static final String REDIS_IDLECONNECTION_TIMEOUT_DOC = "REDIS idleConnectionTimeout.";

  public static final String REDIS_RETRY_ATTEMPTS = "redis.retryAttempts";
  private static final String REDIS_RETRY_ATTEMPTS_DOC = "REDIS retryAttempts.";

  public static final String REDIS_PASSWORD = "redis.password";
  private static final String REDIS_PASSWORD_DOC = "REDIS password.";

  public static final String REDIS_SUBSCRIPTION_PER_CONNECTION = "redis.subscriptionsPerConnection";
  private static final String REDIS_SUBSCRIPTION_PER_CONNECTION_DOC = "REDIS subscriptionsPerConnection.";

  public static final String REDIS_ADDRESS = "redis.address";
  private static final String REDIS_ADDRESS_DOC = "REDIS address.";

  public static final String REDIS_DATABASE = "redis.database";
  private static final String REDIS_DATABASE_DOC = "REDIS database.";

  public static final String REDIS_CONNECTIONPOOLSIZE= "redis.connectionPoolSize";
  private static final String REDIS_CONNECTIONPOOLSIZE_DOC = "REDIS connectionPoolSize.";

  public static final String REDIS_THREADS= "redis.threads";
  private static final String REDIS_THREADS_DOC = "REDIS threads.";

  public static final String REDIS_CONNECTION_MINIMUN_IDLE_SIZE= "redis.connectionMinimumIdleSize";
  private static final String REDIS_CONNECTION_MINIMUN_IDLE_SIZE_DOC = "REDIS threads.";


  public SinkConnectorConfig(ConfigDef config, Map<String, String> parsedConfig) {
    super(config, parsedConfig);
  }

  public SinkConnectorConfig(Map<String, String> parsedConfig) {
    this(conf(), parsedConfig);
  }

  public static ConfigDef conf() {
    return new ConfigDef()
            .define(REDIS_CONNECT_TIMEOUT, Type.INT, Importance.HIGH, REDIS_CONNECT_TIMEOUT_DOC)
            .define(REDIS_TIMEOUT, Type.INT, Importance.HIGH, REDIS_TIMEOUT_DOC)
            .define(REDIS_IDLECONNECTION_TIMEOUT, Type.INT, Importance.HIGH, REDIS_IDLECONNECTION_TIMEOUT_DOC)
            .define(REDIS_RETRY_ATTEMPTS, Type.INT, Importance.HIGH, REDIS_RETRY_ATTEMPTS_DOC)
            .define(REDIS_PASSWORD, Type.STRING, Importance.HIGH, REDIS_PASSWORD_DOC)
            .define(REDIS_ADDRESS, Type.STRING, Importance.HIGH, REDIS_ADDRESS_DOC)
            .define(REDIS_DATABASE, Type.INT, Importance.HIGH, REDIS_DATABASE_DOC)
            .define(REDIS_CONNECTIONPOOLSIZE, Type.INT, Importance.HIGH, REDIS_CONNECTIONPOOLSIZE_DOC)
            .define(REDIS_THREADS, Type.INT, Importance.HIGH, REDIS_THREADS_DOC)
            .define(REDIS_CONNECTION_MINIMUN_IDLE_SIZE, Type.INT, Importance.HIGH, REDIS_CONNECTION_MINIMUN_IDLE_SIZE_DOC)
            .define(REDIS_SUBSCRIPTION_PER_CONNECTION, Type.INT, Importance.HIGH, REDIS_SUBSCRIPTION_PER_CONNECTION_DOC);

  }

  public  Integer getRedisConnectTimeout() {
    return this.getInt(REDIS_CONNECT_TIMEOUT);
  }

  public  Integer getRedisTimeout() {
    return this.getInt(REDIS_TIMEOUT);
  }

  public  Integer getRedisIdleconnectionTimeout() {
    return this.getInt(REDIS_IDLECONNECTION_TIMEOUT);
  }

  public  Integer getRedisRetryAttempts() {
    return this.getInt(REDIS_RETRY_ATTEMPTS);
  }

  public  String getRedisPassword() {
    return this.getString(REDIS_PASSWORD);
  }

  public  String getRedisAddress() {
    return this.getString(REDIS_ADDRESS);
  }

  public  Integer getRedisDatabase() {
    return this.getInt(REDIS_DATABASE);
  }

  public  Integer getRedisConnectionpoolsize() {
    return this.getInt(REDIS_CONNECTIONPOOLSIZE);
  }

  public  Integer getRedisThreads() {
    return this.getInt(REDIS_THREADS);
  }

  public  Integer getRedisConnectionMinimunIdleSize() {
    return this.getInt(REDIS_CONNECTION_MINIMUN_IDLE_SIZE);
  }

  public Integer getRedisSubscriptionPerConnection() {
    return this.getInt(REDIS_SUBSCRIPTION_PER_CONNECTION);
  }
}

package com.noon.breakoutScoreSinkConnector;

import com.noon.breakoutScoreSinkConnector.service.Service;
import com.noon.breakoutScoreSinkConnector.service.ServiceImpl;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.connect.sink.SinkRecord;
import org.apache.kafka.connect.sink.SinkTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class BreakoutSinkTask extends SinkTask {
    private static Logger log = LoggerFactory.getLogger(BreakoutSinkTask.class);
    private Service service;


    @Override
    public String version() {
        return VersionUtil.getVersion();
    }


    @Override
    public void start(Map<String, String> map) {
        log.info("Initual map {}",map);
        service = new ServiceImpl( new SinkConnectorConfig(map));
    }

    @Override
    public void put(Collection<SinkRecord> collection) {
        try {
            log.info("Cosuning from kafka {}",collection);
            Collection<String> recordsAsString = collection.stream().map(r -> String.valueOf(r.value())).collect(Collectors.toList());
            service.process(recordsAsString);
        }
        catch (Exception e) {
            log.error("Error while processing records");
            log.error(e.toString());
        }
    }

    @Override
    public void flush(Map<TopicPartition, OffsetAndMetadata> map) {
        log.trace("Flushing the queue");
    }

    @Override
    public void stop() {
        try {
            service.closeClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

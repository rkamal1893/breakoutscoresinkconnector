FROM 486108188727.dkr.ecr.eu-central-1.amazonaws.com/prod-noon-datalake-dl-kafka-connect:5.4.0-beta1-v0.16

ARG JAR_FILE=target/*.jar
RUN mkdir -p /usr/share/java/connect-transformer
COPY $JAR_FILE /usr/share/java/connect-transformer/
RUN cd /usr/share/java/connect-transformer/ \
&& curl https://repo1.maven.org/maven2/com/vdurmont/emoji-java/5.1.1/emoji-java-5.1.1.jar --output emoji-java-5.1.1.jar \
&& curl https://repo1.maven.org/maven2/org/json/json/20200518/json-20200518.jar --output json-20200518.jar
#RUN ls -altr /usr/share/java/*/*